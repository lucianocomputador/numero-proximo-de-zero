/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temperaturaproximozero;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author luciano
 */
public class TemperaturaLuciano {

    public static void main(String[] args) {
        int[] n = {};
        Scanner myObj = new Scanner(System.in);
        String entrada;

        System.out.println("Entre com os valores. Exemplo: 17, 32, 14, 21");
        entrada = myObj.nextLine();

        try {

            String[] numSeparado = entrada.split(",");

            for (String num : numSeparado) {
                n = addElement(n, Integer.parseInt(num.trim()));
            }

            if (n.length > 0) {
                TemperaturaFind find = new TemperaturaFind();
                System.out.println(find.argMin(n));
            } else {
                System.out.println("Lista sem números");
            }

        } catch (Exception e) {
            System.out.println("Algo deu errado na entrada dos números");
        }
        

    }

    static int[] addElement(int[] a, int e) {
        a = Arrays.copyOf(a, a.length + 1);
        a[a.length - 1] = e;
        return a;
    }
}
