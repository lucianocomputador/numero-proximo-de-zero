/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temperaturaproximozero;

import java.util.List;

/**
 *
 * @author luciano
 */
public class TemperaturaFind {

    /**
     * Encontra a menor numero mais proxímo de zero
     *
     * @param a
     * @return
     */
    public int argMin(int[] a) {
        int v = Integer.MAX_VALUE;
        int ind = -1;
        int valorA = -1;
        int valorV = -1;
        for (int i = 0; i < a.length; i++) {
            valorA = a[i];
            valorV = v;

            // Tudo posotivo para comparação
            if (a[i] < 0) {
                valorA = (a[i] * (-1));
            }
            if (v < 0) {
                valorV = (v * (-1));
            }

            if ((valorA) == valorV) {
                // Se igual ficar com o maior
                if (a[i] > v) {
                    v = a[i];
                    ind = i;
                }
            } else if ((valorA) < valorV) {
                v = a[i];
                ind = i;
            }
        }

        return a[ind];
    }

}
