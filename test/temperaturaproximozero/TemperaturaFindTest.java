/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temperaturaproximozero;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author luciano
 */
public class TemperaturaFindTest {

    TemperaturaFind instance = new TemperaturaFind();

    public TemperaturaFindTest() {
    }

    /**
     * Test of argMin method, of class TemperaturaFind.
     */
    @Test
    public void test01ArgMin() {
        System.out.println("Teste 1");
        int[] temperatura = {17, 32, 14, 21};
        int expResult = 14;
        int result = instance.argMin(temperatura);
        assertEquals(expResult, result);
    }

    @Test
    public void test02ArgMin() {
        System.out.println("Teste 2");
        int[] temperatura = {27, -8, -12, 9};
        int expResult = -8;
        int result = instance.argMin(temperatura);
        assertEquals(expResult, result);
    }
    
    @Test
    public void test03ArgMin() {
        System.out.println("Teste 3");
        int[] temperatura = {-6, 14, 42, 6, 25, -18};
        int expResult = 6;
        int result = instance.argMin(temperatura);
        assertEquals(expResult, result);
    }
    
    @Test
    public void test04ArgMin() {
        System.out.println("Teste 4");
        int[] temperatura = {34, 11, 13, -23, -11, 18};
        int expResult = 11;
        int result = instance.argMin(temperatura);
        assertEquals(expResult, result);
    }
    
    @Test
    public void test05ArgMin() {
        System.out.println("Teste 5");
        int[] temperatura = {17, 0, 28, -4};
        int expResult = 0;
        int result = instance.argMin(temperatura);
        assertEquals(expResult, result);
    }

    @Test
    public void test06ArgMin() {
        System.out.println("Teste 6");
        int[] temperatura = {-10, 27, 9, -12};
        int expResult = 9;
        int result = instance.argMin(temperatura);
        assertEquals(expResult, result);
    }
    
    @Test
    public void test07ArgMin() {
        System.out.println("Teste 7");
        int[] temperatura = {-47, -14, -5, -12, -8};
        int expResult = -5;
        int result = instance.argMin(temperatura);
        assertEquals(expResult, result);
    }

    @Test
    public void test08ArgMin() {
        System.out.println("Teste 8");
        int[] temperatura = {-47, -14, -5, -12, -5};
        int expResult = -5;
        int result = instance.argMin(temperatura);
        assertEquals(expResult, result);
    }
    
    @Test
    public void test09ArgMin() {
        System.out.println("Teste 9");
        int[] temperatura = {-7, 12, -13, 8};
        int expResult = -7;
        int result = instance.argMin(temperatura);
        assertEquals(expResult, result);
    }

}
